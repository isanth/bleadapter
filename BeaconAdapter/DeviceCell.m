//
//  DeviceCell.m
//  BeaconAdapter
//
//  Created by Kamil Michalik on 20/05/16.
//  Copyright © 2016 Kamil Michalik. All rights reserved.
//

#import "DeviceCell.h"

@implementation DeviceCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.clipsToBounds = NO;
    self.layer.shadowOpacity = 0.4f;
    self.layer.shadowRadius = 2.0f;
    self.layer.shadowOffset = CGSizeZero;
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.bounds];
    self.layer.shadowPath = shadowPath.CGPath;
    
    self.avatar.layer.cornerRadius = self.avatar.frame.size.width/2;
    self.avatar.image = [self.avatar.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
   // self.avatar.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.avatar.clipsToBounds = YES;
    //self.avatar.layer.borderWidth = 1.f;
    //self.avatar.layer.backgroundColor = [[UIColor blackColor]CGColor];
}



@end
