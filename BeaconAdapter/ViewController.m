//
//  ViewController.m
//  BeaconAdapter
//
//  Created by Kamil Michalik on 25/04/16.
//  Copyright © 2016 Kamil Michalik. All rights reserved.
//

#import "ViewController.h"
#import "LidlBeaconManager.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>
#import "DeviceCell.h"

static NSString * const customSeriviceUUID = @"1804";

@interface ViewController () <CBPeripheralDelegate, CBPeripheralManagerDelegate, CBCentralManagerDelegate, CLLocationManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CBCentralManager *manager;
@property (strong, nonatomic) CBPeripheralManager *peripheralManager;
@property (nonatomic, strong) LidlBeaconManager *bleManager;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) CBPeripheral *peripheral;
@property (strong, nonatomic) NSMutableDictionary *mDict;

@property (weak, nonatomic) IBOutlet UILabel *bleStatusLabel;


@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;


//{
//@private

//
//CBService			*temperatureAlarmService;
//
//CBCharacteristic    *tempCharacteristic;
//CBCharacteristic	*minTemperatureCharacteristic;
//CBCharacteristic    *maxTemperatureCharacteristic;
//CBCharacteristic    *alarmCharacteristic;
//
//CBUUID              *temperatureAlarmUUID;
//CBUUID              *minimumTemperatureUUID;
//CBUUID              *maximumTemperatureUUID;
//CBUUID              *currentTemperatureUUID;
//
////id<LeTemperatureAlarmProtocol>	peripheralDelegate;
//}
@end

@implementation ViewController
{
    CBCharacteristic *_characteristic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _mDict = @{}.mutableCopy;
    
    [self start];

    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) start
{
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        [self.locationManager requestAlwaysAuthorization];
        
        NSLog(@"loaded test view for finding beacons using core bluetooth");
        
        
        
        _manager = [[CBCentralManager alloc] initWithDelegate:self
                                                queue:nil];
        _manager.delegate = self;
        
        
    }
    
//    CBUUID	*serviceUUID	= [CBUUID UUIDWithString:kTemperatureServiceUUIDString];
//    NSArray	*serviceArray	= [NSArray arrayWithObjects:serviceUUID, nil];
    
    
    //[servicePeripheral discoverServices:@[]];

  //  [self.bleManager startMonitoring];
    
    
//    
//    


}



#pragma mark - Core Bluetooth Central Delegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    
   // 0C4DC164-700A-4DB6-2674-2083F98D7783
    
    CBUUID *tvServiceUUID = [CBUUID UUIDWithString:@"D00D"]; // fenix
    NSArray *serivces = @[tvServiceUUID];
    
    
    // Determine the state of the peripheral
    if ([central state] == CBCentralManagerStatePoweredOff) {
        NSLog(@"CoreBluetooth BLE hardware is powered off");
    }
    else if ([central state] == CBCentralManagerStatePoweredOn) {
        NSLog(@"CoreBluetooth BLE hardware is powered on and ready");
        
        NSDictionary *options = @{
                                  CBCentralManagerScanOptionAllowDuplicatesKey: @NO
                                  };
        
        
        
        
        [_manager scanForPeripheralsWithServices:serivces
                                         options:options];
        
        
        
        
        NSLog(@"I just started scanning for peripherals");
        
        self.bleStatusLabel.text = @"Detecting BLE devices ...";
        
    }
    else if ([central state] == CBCentralManagerStateUnauthorized) {
        NSLog(@"CoreBluetooth BLE state is unauthorized");
    }
    else if ([central state] == CBCentralManagerStateUnknown) {
        NSLog(@"CoreBluetooth BLE state is unknown");
    }
    else if ([central state] == CBCentralManagerStateUnsupported) {
        NSLog(@"CoreBluetooth BLE hardware is unsupported on this platform");
    }
    
    
    
    
}


- (void) centralManager:(CBCentralManager *)central
    didDiscoverPeripheral:(CBPeripheral *)peripheral
        advertisementData:(NSDictionary *)advertisementData
                     RSSI:(NSNumber *)RSSI
{
    NSLog(@"I see an advertisement with identifer: %@, state: %ld, name: %@, services: %@, description: %@",
          [peripheral identifier],
          (long)[peripheral state],
          [peripheral name],
          [peripheral services],
          [advertisementData description]);
    
    if (_peripheral == nil)
    {
        NSLog(@"Trying to connect to peripheral");
        _peripheral = peripheral;
        _peripheral.delegate = (id)self;
        [central connectPeripheral:peripheral
                           options:nil];
    }
    
     NSString *deviceName = [peripheral name];
    [_mDict setValue:deviceName forKey:[peripheral identifier].UUIDString];
    
    NSLog(@"Devices: %@", _mDict );
    
    
    
    [self.collectionView reloadData];
    
}

- (void) centralManager:(CBCentralManager *)central
    didConnectPeripheral:(CBPeripheral *)peripheral
{
    if (peripheral == nil)
    {
        NSLog(@"connect callback has nil peripheral");
    }
    else {
        
        UIAlertController *alertController2 = [UIAlertController alertControllerWithTitle:@"Connection established!"
                                                                                  message:@"Discovered devices has been connected!"
                                                                           preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                     style:UIAlertActionStyleDefault
                                                   handler:nil];
        
        
        
        
        
        [alertController2 addAction:ok];
        [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:alertController2 animated:YES completion:nil];
        
    
        NSLog(@"Connected to peripheral with identifer: %@, state: %ld, name: %@, services: %@",
              [peripheral identifier],
              (long)[peripheral state],
              [peripheral name],
              [peripheral services]);
        
        NSLog(@"discovering services...");
        _peripheral = peripheral;
        _peripheral.delegate = self;
        [_peripheral discoverServices:nil];
    }

}

#pragma mark - Core Bluetooth Peripheral Delegate

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSArray *)serviceUuids
{
    NSLog(@"Services scanned !");
    for (CBService *s in peripheral.services)
    {
        NSLog(@"Service found : %@",s.UUID);
        
        [peripheral discoverCharacteristics:nil forService:s];
    }
}


-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    NSLog(@"discovered a Characteristics's service: %@", service);
    
    // Again, we loop through the array, just in case.
    for (CBCharacteristic *characteristic in service.characteristics) {
        
        NSLog(@"Discovered characteristic %@", characteristic);
        
        
        // And check if it's the right one
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"A1EA811A-0E1B-D4A1-B840-63F88C8DA1EA"]]) {
            
             _characteristic = characteristic;
            
            // If it is, subscribe to it
            [_peripheral setNotifyValue:YES forCharacteristic:characteristic];
       // NSLog(@"Reading value for characteristic %@", characteristic);
            [_peripheral readValueForCharacteristic:characteristic];
        }
    }
    
}

-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(nonnull CBCharacteristic *)characteristic error:(nullable NSError *)error
{
    //if  (characteristic.properties == CBCharacteristicPropertyRead) {
   
    NSData *data = characteristic.value;
    // parse the data as needed
  
    NSLog(@"Characteristic value : %@ with ID %@", characteristic.value, characteristic.UUID);
    
    //NSLog(@"Characs DATA: %lu", (unsigned long)characteristic.properties );
   // }
}



// Check if discovered peripheral state and has bluetooth switch ON
-(void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
{
    NSLog(@"Peripheral advertising : %@",peripheral.description) ;
  
        if (peripheral.state == CBPeripheralManagerStatePoweredOn) {
            NSLog(@"Peripheral BLE is ON");
        }

}


//- (void) peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
//{
//    NSArray		*services	= nil;
//    NSArray		*uuids	= [NSArray arrayWithObjects:currentTemperatureUUID, // Current Temp
//                           minimumTemperatureUUID, // Min Temp
//                           maximumTemperatureUUID, // Max Temp
//                           temperatureAlarmUUID, // Alarm Characteristic
//                           nil];
//    
//    if (peripheral != servicePeripheral) {
//        NSLog(@"Wrong Peripheral.\n");
//        return ;
//    }
//    
//    if (error != nil) {
//        NSLog(@"Error %@\n", error);
//        return ;
//    }
//    
//    services = [peripheral services];
//    if (!services || ![services count]) {
//        return ;
//    }
//    
//    temperatureAlarmService = nil;
//    
//    for (CBService *service in services) {
//        if ([[service UUID] isEqual:[CBUUID UUIDWithString:kTemperatureServiceUUIDString]]) {
//            temperatureAlarmService = service;
//            break;
//        }
//    }
//    
//    if (temperatureAlarmService) {
//        [peripheral discoverCharacteristics:uuids forService:temperatureAlarmService];
//    }
    

//}

#pragma mark - Core Location Delegate

-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    CLBeacon *beacon = [beacons lastObject];
    NSLog(@"Proximity UUID: [%@]", beacon.proximityUUID.UUIDString);
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_mDict allKeys].count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"devicesCell";
    
    DeviceCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.deviceName.text = [_mDict allValues][indexPath.row];
    
    return cell;
}

- (IBAction)bleButtonStatus:(UIButton *)button
{
    CBUUID *tvServiceUUID = [CBUUID UUIDWithString:@"D00D"]; // fenix
    NSArray *serivces = @[tvServiceUUID];
    
    if (!button.isSelected) {
        [_manager retrievePeripheralsWithIdentifiers:serivces];
        [self start];
        [button setSelected:YES];
    }
    else {
        [_peripheral setNotifyValue:NO forCharacteristic:_characteristic];
        [_manager cancelPeripheralConnection:_peripheral];
        self.bleStatusLabel.text = @"Stopped monitoring for devices";
        [button setSelected:NO];
    }
}

@end
