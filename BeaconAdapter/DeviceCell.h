//
//  DeviceCell.h
//  BeaconAdapter
//
//  Created by Kamil Michalik on 20/05/16.
//  Copyright © 2016 Kamil Michalik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *deviceName;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;

@end
