//
//  LidlBeaconManager.h
//  BeaconReceiver
//
//  Created by Piotr Sarnowski on 13/02/16.
//  Copyright © 2016 Upnext. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

//constants and defs
extern NSString * const kLidlBeaconNotificationName;
extern NSString * const kLidlBeaconNotificationRegionIDKey;

extern NSString * const kLidlBeaconCheckoutRegionName;
extern NSString * const kLidlBeaconEntryRegionName;

//protocol
@protocol LidlBeaconManagerDelegate <NSObject>

@optional
- (void) beaconActionShouldTriggerForRegion:(NSString * const) regionName;

@end

//interface
@interface LidlBeaconManager : NSObject<CLLocationManagerDelegate>

- (void) requestAuth;

- (void) startMonitoring;

@property (nonatomic, weak) id<LidlBeaconManagerDelegate> delegate;

@end
