//
//  AppDelegate.h
//  BeaconAdapter
//
//  Created by Kamil Michalik on 25/04/16.
//  Copyright © 2016 Kamil Michalik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

