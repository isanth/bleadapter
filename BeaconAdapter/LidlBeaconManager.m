//
//  LidlBeaconManager.m
//  BeaconReceiver
//
//  Created by Piotr Sarnowski on 13/02/16.
//  Copyright © 2016 Upnext. All rights reserved.
//

#import "LidlBeaconManager.h"

NSString * const kLidlBeaconNotificationName = @"kLidlBeaconNotificationName";
NSString * const kLidlBeaconNotificationRegionIDKey = @"kLidlBeaconNotificationRegionIDKey";
//
static NSString * const kLidlBeaconCheckoutBeaconUUID = @"CBBA9ADB-651D-4B5E-8E7C-53752B0195C7";
NSString * const kLidlBeaconCheckoutRegionName = @"com.lidldemo.checkoutRegion";
//
NSString * const kLidlBeaconEntryRegionName = @"com.lidldemo.entryRegion";
static NSString * const kLidlBeaconEntryBeaconUUID = @"EE3B717F-B747-4F6F-919E-2A29C282AB1F";

@interface LidlBeaconManager()

@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion;
@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) NSString * lastTriggeredBeaconRegionIdentifier;

@property (assign) BOOL logProximityAlerts;

@end

@implementation LidlBeaconManager

- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        
        _logProximityAlerts = YES;
        
        [self requestAuth];
    }
    
    return self;
}

- (void) startMonitoring
{
 
    
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways)
    {
        
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Beacon monitoring not authorized!"
                                                                                 message:@""
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                     style:UIAlertActionStyleDefault
                                                   handler:nil];
        
        
        
        
        
        [alertController addAction:ok];
        
        [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
    
    if (![CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]])
    {
        
        UIAlertController *alertController2 = [UIAlertController alertControllerWithTitle:@"Monitoring not available!"
                                                                                  message:@""
                                                                           preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                     style:UIAlertActionStyleDefault
                                                   handler:nil];
        
        
        
        
        
        [alertController2 addAction:ok];
        [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:alertController2 animated:YES completion:nil];
        return;
    }
    
    //region 1
    // Create a NSUUID with the same UUID as the broadcasting beacon
    NSUUID *uuid1;// = [[NSUUID alloc] initWithUUIDString:kLidlBeaconEntryBeaconUUID];
    
    // Setup a new region with that UUID and same identifier as the broadcasting beacon
    CLBeaconRegion * region1 = [[CLBeaconRegion alloc] initWithProximityUUID:uuid1
                                                                  identifier:kLidlBeaconEntryRegionName];
    
    // Tell location manager to start monitoring for the beacon region
    region1.notifyOnEntry = YES;
    region1.notifyOnExit = YES;
    region1.notifyEntryStateOnDisplay = YES;
    
    [self.locationManager startMonitoringForRegion:region1];
    
    // Create a NSUUID with the same UUID as the broadcasting beacon
    NSUUID *uuid2;// = [[NSUUID alloc] initWithUUIDString:kLidlBeaconCheckoutBeaconUUID];
    
    // Setup a new region with that UUID and same identifier as the broadcasting beacon
    CLBeaconRegion * region2 = [[CLBeaconRegion alloc] initWithProximityUUID:uuid2
                                                                  identifier:kLidlBeaconCheckoutRegionName];
    
    // Tell location manager to start monitoring for the beacon region
    region2.notifyOnEntry = YES;
    region2.notifyOnExit = YES;
    region2.notifyEntryStateOnDisplay = YES;
    
    [self.locationManager startMonitoringForRegion:region2];
}

- (void) requestAuth
{
    /*
     Remember to include the NSLocationAlwaysUsageDescription key in info.plist!
     */
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
    {
        NSLog(@"LIDLBEACON: AUTH kCLAuthorizationStatusNotDetermined");
        
        [self.locationManager requestAlwaysAuthorization];
    }
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) NSLog(@"LIDLBEACON: AUTH = kCLAuthorizationStatusDenied");
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted) NSLog(@"LIDLBEACON: AUTH = kCLAuthorizationStatusRestricted");
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized) NSLog(@"LIDLBEACON: AUTH = kCLAuthorizationStatusAuthorized"); [self startMonitoring ];
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) NSLog(@"LIDLBEACON: AUTH = kCLAuthorizationStatusAuthorizedAlways");
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) NSLog(@"LIDLBEACON: AUTH = kCLAuthorizationStatusAuthorizedWhenInUse");
    
}

#pragma mark - CLLocationManagerDelegate Protocol

- (void) locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    [self.locationManager requestStateForRegion:region];
}

-(void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    if (![region isKindOfClass:[CLBeaconRegion class]]) return;
    
    if (state == CLRegionStateInside)
    {
        //Start Ranging
        [manager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
    }
    else
    {
        //Stop Ranging here
        [manager stopRangingBeaconsInRegion:(CLBeaconRegion *)region];
    }
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    NSLog(@"LIDLBEACON: Entered region: %@", region.identifier);
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    NSLog(@"LIDLBEACON: Exited region: %@", region.identifier);
}

-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    if(beacons.count > 0)
    {
        //beacon found!
        CLBeacon *foundBeacon = [beacons firstObject];
        NSString *uuid = foundBeacon.proximityUUID.UUIDString;
        
        if (_logProximityAlerts)
        {
            NSLog(@"LIDLBEACON: PROXIMITY ALERT: %@, proximity: %ld", uuid, foundBeacon.proximity);
        }
        
        if (foundBeacon.proximity == CLProximityImmediate || foundBeacon.proximity == CLProximityNear || foundBeacon.proximity == CLProximityFar)
        {
            if (![region.identifier isEqualToString:_lastTriggeredBeaconRegionIdentifier])
            {
                //memorize the region
                _lastTriggeredBeaconRegionIdentifier = region.identifier;
                
                //post notification
             //   [[NSNotificationCenter defaultCenter] postNotificationName:kLidlBeaconNotificationName
               //                                                     object:self
                 //                                                 userInfo:@{kLidlBeaconNotificationRegionIDKey : /region.identifier}];
                
                //trigger delegate
                if ([self.delegate respondsToSelector:@selector(beaconActionShouldTriggerForRegion:)])
                {
                    [self.delegate beaconActionShouldTriggerForRegion:region.identifier];
                }
            }
        }
    }
}



@end
